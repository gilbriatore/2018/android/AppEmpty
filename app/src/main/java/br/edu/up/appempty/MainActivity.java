package br.edu.up.appempty;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

  int numero = 10;
  double valor = 100;
  boolean aprovado = true;



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    //Recebe a mensagem da intent.
    Intent in = getIntent();
    String msg = in.getStringExtra("Identificador");

    //Faz alguma coisa com a mensagem...


    //Estrutura if-else
    if (numero > 10){
      //faz um coisa;
    } else {
      //senão faz outra;
    }

    //Estrutura swicth-case
    switch (numero){
      case 10:
        //faz um coisa;
        break;
      case 20:
        //faz outra;
        break;
      default:
        //senão faz essa;
        break;
    }

    int[] notas = new int[5];
    notas[0] = 5;
    notas[1] = 7;
    notas[2] = 9;
    notas[3] = 3;
    notas[4] = 8;

    int contador = 0;
    while(contador < notas.length){
      contador++;
      int nota = notas[contador];
      //faz alguma operação com a nota...
    }

    for (int i = 0; i < notas.length ; i++) {
      int nota = notas[i];
      //faz alguma operação com a nota...
    }

    ArrayList<String> nomes = new ArrayList<>();
    nomes.add("Ana");
    nomes.add("Pedro");
    nomes.add("Paulo");
    nomes.add("Ivo");

    for (String nome : nomes) {
        //faz alguma operação com o nome
    }


    Button botao = new Button(this);
    botao.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
         //realiza alguma ação após o clique no botão...
      }
    });

    botao.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
      }
    });

    botao.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View view) {
        return false;
      }
    });

    botao.setOnDragListener(new View.OnDragListener() {
      @Override
      public boolean onDrag(View view, DragEvent dragEvent) {
        return false;
      }
    });


    enviarMensagem(null);
  }


  public void onClick(View v){
    //realiza alguma ação após o clique no botão...
  }


  public void enviarMensagem(View v) {

    String mensagem =  "Olá, seja bem-vindo!";
    Intent intent = new Intent(this, DestinoActivity.class);
    intent.putExtra("Identificador", mensagem);
    startActivity(intent);

  }

}
